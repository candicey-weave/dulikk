# Dulikk
**Dulikk** is a [Weave](https://github.com/Weave-MC) mod that allows you to customise the way your hold items are displayed.

<br>

## Images
![Ingame](assets/ingame.png)
![Settings](assets/settings-gui.png)

<br>

## Installation
To install Dulikk, follow the steps below:

1. Download the [Dulikk mod](#download).
2. Place the `.jar` file into your Weave mods directory:
    - **Windows**: `%userprofile%\.weave\mods` or `%userprofile%\.weave\mods\1.8.9`
    - **Unix**: `~/.weave/mods` or `~/.weave/mods/1.8.9`

<br>

## Usage
To open the mod's settings, run the following command in-game:

```
/dulikk gui
```

<br>

## Download
The latest version of the mod can be downloaded from the [Package Registry](https://gitlab.com/candicey-weave/dulikk/-/packages). Select the most recent version and download the `.jar` file that ends with `-relocated.jar`.

<br>

## Build Instructions
To build Dulikk from source:

1. Clone the repository to your local machine.
2. In the root directory of the repository, run the following command:

   ```
   ./gradlew cleanBuild
   ```

3. After the build process completes, the compiled `.jar` files will be located in the `build/libs` directory.

<br>

## Credits
- [DulkirMod](https://github.com/inglettronald/DulkirMod) - For the original idea and code.
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.

<br>

## License
- Dulikk is licensed under the [GNU Affero General Public License Version 3](LICENSE).