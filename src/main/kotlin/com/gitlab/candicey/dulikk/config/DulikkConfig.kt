package com.gitlab.candicey.dulikk.config

import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigProperties.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigProperties.Text.Number

data class DulikkConfig(
    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomHandAnimations")
    @DisplayText("Custom Animations | Change the look of your held item. (default: True)")
    var customHandAnimations: Boolean = true,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomHeldItemSize")
    @DisplayText("Custom Size | Scales the size of your currently held item. (default: 0)")
    var customHeldItemSize: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setDoesScaleSwing")
    @DisplayText("Scale Swing | Also scale the size of the swing animation. (default: False)")
    var doesScaleSwing: Boolean = false,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomX")
    @DisplayText("Custom X | Moves the held item. (default: 0)")
    var customX: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomY")
    @DisplayText("Custom Y | Moves the held item. (default: 0)")
    var customY:Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomZ")
    @DisplayText("Custom Z | Moves the held item. (default: 0)")
    var customZ: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomYaw")
    @DisplayText("Custom Yaw | Moves the held item. (default: 0)")
    var customYaw: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomPitch")
    @DisplayText("Custom Pitch | Moves the held item. (default: 0)")
    var customPitch: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomRoll")
    @DisplayText("Custom Roll | Moves the held item. (default: 0)")
    var customRoll: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setCustomSpeed")
    @DisplayText("Custom Speed | Speed of the swing animation. (default: 0)")
    var customSpeed: Float = 0.0f,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setIgnoreHaste")
    @DisplayText("Ignore Haste | Makes the chosen speed override haste modifiers. (default: False)")
    var ignoreHaste: Boolean = false,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setRightHand")
    @DisplayText("Main Hand | Set to true to use the right hand; otherwise, use the left. (default: True)")
    var rightHand: Boolean = true,

    @Shadow.Method(CONFIG_VALUE_CLASS_NAME, "setDrinkingSelector")
    @DisplayText("Drinking Fix | Pick how to handle drinking animations. (0 = No Fix, 1= Rotationless, 2 = Fixed (default))")
    @Number.Min(0.0f)
    @Number.Max(2.0f)
    @Text.MaxLength(1)
    var drinkingSelector: Int = 2,
)
