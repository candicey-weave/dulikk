package com.gitlab.candicey.dulikk.config

import com.gitlab.candicey.dulikk.dulikkConfig

internal const val CONFIG_VALUE_CLASS_NAME = "com/gitlab/candicey/dulikk/config/ConfigValueKt"

var customHandAnimations: Boolean by dulikkConfig.delegateValue()

var customHeldItemSize: Float by dulikkConfig.delegateValue()

var doesScaleSwing: Boolean by dulikkConfig.delegateValue()

var customX: Float by dulikkConfig.delegateValue()

var customY: Float by dulikkConfig.delegateValue()

var customZ: Float by dulikkConfig.delegateValue()

var customYaw: Float by dulikkConfig.delegateValue()

var customPitch: Float by dulikkConfig.delegateValue()

var customRoll: Float by dulikkConfig.delegateValue()

var customSpeed: Float by dulikkConfig.delegateValue()

var ignoreHaste: Boolean by dulikkConfig.delegateValue()

var rightHand: Boolean by dulikkConfig.delegateValue()

var drinkingSelector: Int by dulikkConfig.delegateValue()