package com.gitlab.candicey.dulikk

import com.gitlab.candicey.dulikk.command.impl.SettingsCommand
import com.gitlab.candicey.dulikk.hook.EntityLivingBaseHook
import com.gitlab.candicey.dulikk.hook.ItemRendererHook
import com.gitlab.candicey.zenithcore.extension.registerHook
import com.gitlab.candicey.zenithloader.ZenithLoader
import net.weavemc.api.ModInitializer
import net.weavemc.loader.InjectionHandler
import java.lang.instrument.Instrumentation

class DulikkMain : ModInitializer {
    @Suppress("OVERRIDE_DEPRECATION")
    override fun preInit(inst: Instrumentation) {
        ZenithLoader.loadDependencies("dulikk", inst = inst)

        arrayOf(
            ItemRendererHook,
            EntityLivingBaseHook,
        ).forEach(InjectionHandler::registerHook)

        commandManager.registerCommand(
            SettingsCommand,
        )
    }

    override fun init() {
        configManager.init()
    }
}