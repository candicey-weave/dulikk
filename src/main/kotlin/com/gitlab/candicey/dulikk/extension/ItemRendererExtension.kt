package com.gitlab.candicey.dulikk.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.renderer.ItemRenderer
import net.minecraft.item.ItemStack

var ItemRenderer.itemToRender: ItemStack by ShadowField()