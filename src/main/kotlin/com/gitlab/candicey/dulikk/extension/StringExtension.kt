package com.gitlab.candicey.dulikk.extension

import com.gitlab.candicey.zenithcore.util.GREY
import com.gitlab.candicey.zenithcore.util.LIGHT_PURPLE
import com.gitlab.candicey.zenithcore.util.YELLOW

fun String.addDulikkPrefix() = "${GREY}[${LIGHT_PURPLE}Dulikk${GREY}]$YELLOW $this"