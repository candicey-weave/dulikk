package com.gitlab.candicey.dulikk.hook

import com.gitlab.candicey.dulikk.ItemAnimations
import com.gitlab.candicey.dulikk.extension.itemToRender
import com.gitlab.candicey.zenithcore.util.callStaticsBooleanIf
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.minecraft.client.entity.AbstractClientPlayer
import net.minecraft.client.renderer.ItemRenderer
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodInsnNode

/**
 * @see [net.minecraft.client.renderer.ItemRenderer]
 */
object ItemRendererHook : Hook("net/minecraft/client/renderer/ItemRenderer") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStaticsBooleanIf<ItemRendererHook>(
            "transformFirstPersonItem",
            "doItemUsedTransformations",
            "performDrinking",
        )

        with(node.methods.named("renderItemInFirstPerson")) {
            val pushMatrix = instructions.find { it.opcode == Opcodes.INVOKESTATIC && with(it as MethodInsnNode) { owner == "net/minecraft/client/renderer/GlStateManager" && name == "pushMatrix" && desc == "()V" } }
            instructions.insert(pushMatrix, asm {
                invokestatic(internalNameOf<ItemRendererHook>(), ::onRenderItemInFirstPerson.name, "()V")
            })
        }

        cfg.computeFrames()
    }

    @JvmStatic
    fun onTransformFirstPersonItem(itemRenderer: ItemRenderer, equipProgress: Float, swingProgress: Float): Boolean =
        ItemAnimations.itemTransforHook(equipProgress, swingProgress)

    @JvmStatic
    fun onDoItemUsedTransformations(itemRenderer: ItemRenderer, swingProgress: Float): Boolean =
        ItemAnimations.scaledSwing(swingProgress)

    @JvmStatic
    fun onPerformDrinking(itemRenderer: ItemRenderer, clientPlayer: AbstractClientPlayer, partialTicks: Float): Boolean {
        var cancel = false

        if (ItemAnimations.rotationlessDrink(clientPlayer, partialTicks)) {
            cancel = true
        }

        if (ItemAnimations.scaledDrinking(clientPlayer, partialTicks, itemRenderer.itemToRender)) {
            cancel = true
        }

        return cancel
    }

    @JvmStatic
    fun onRenderItemInFirstPerson() = ItemAnimations.changeMainHand()
}