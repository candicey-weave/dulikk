package com.gitlab.candicey.dulikk.hook

import com.gitlab.candicey.dulikk.ItemAnimations
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.minecraft.entity.EntityLivingBase
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.tree.ClassNode

/**
 * @see [net.minecraft.entity.EntityLivingBase]
 */
object EntityLivingBaseHook : Hook("net/minecraft/entity/EntityLivingBase") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val instructions = node.methods.named("getArmSwingAnimationEnd").instructions

        instructions.clear()
        instructions.insert(asm {
            aload(0)
            invokestatic(
                internalNameOf<EntityLivingBaseHook>(),
                "onGetArmSwingAnimationEnd",
                "(Lnet/minecraft/entity/EntityLivingBase;)I",
            )
            ireturn
        })

        cfg.computeFrames()
    }

    @JvmStatic
    fun onGetArmSwingAnimationEnd(entityLivingBase: EntityLivingBase): Int = ItemAnimations.onGetArmSwingAnimationEnd(entityLivingBase)
}