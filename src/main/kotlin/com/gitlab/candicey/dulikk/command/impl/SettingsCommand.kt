package com.gitlab.candicey.dulikk.command.impl

import com.gitlab.candicey.dulikk.command.Entry
import com.gitlab.candicey.dulikk.dulikkConfig
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigGui
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigOptionsGenerator
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.GuiScreenHelper

@CommandInfo("settings", "setting", "stg", "s", "gui", "g")
object SettingsCommand : Entry() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        GuiScreenHelper.addGuiOpenQueue(
            ConfigGui(
                "Dulikk Settings",
                ConfigOptionsGenerator.generateConfigOptions(dulikkConfig.config)
            )
        )
    }
}