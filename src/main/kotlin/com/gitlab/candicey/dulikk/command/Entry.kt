package com.gitlab.candicey.dulikk.command

import com.gitlab.candicey.dulikk.commandInitialisationData
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract

open class Entry : CommandAbstract(commandInitialisationData)