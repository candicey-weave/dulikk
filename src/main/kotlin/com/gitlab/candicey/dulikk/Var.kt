package com.gitlab.candicey.dulikk

import com.gitlab.candicey.dulikk.config.DulikkConfig
import com.gitlab.candicey.dulikk.extension.addDulikkPrefix
import com.gitlab.candicey.zenithcore.config.Config
import com.gitlab.candicey.zenithcore.config.ConfigManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInitialisationData
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandManager

internal val configManager = ConfigManager(
    mutableMapOf(
        "dulikkconfig" to Config(resolveConfigPath("dulikk.json"), DulikkConfig())
    )
)

internal val dulikkConfig: Config<DulikkConfig> by configManager.delegateConfig<DulikkConfig>()

internal val commandInitialisationData by lazy { CommandInitialisationData(listOf("dulikk"), "/", String::addDulikkPrefix) }
internal val commandManager by lazy { CommandManager(commandInitialisationData) }
