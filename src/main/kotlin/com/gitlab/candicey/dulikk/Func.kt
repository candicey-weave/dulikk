package com.gitlab.candicey.dulikk

import java.io.File

internal fun resolveConfigPath(fileName: String): File {
    val directory = File(System.getProperty("user.home"), ".weave/Dulikk")
    if (!directory.exists()) {
        directory.mkdirs()
    }
    return File(directory, fileName)
}
